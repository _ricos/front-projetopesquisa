import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppService } from '../app.service';
import { FormGroup } from '@angular/forms';
import { map } from "rxjs/operators";
import { baseUrl } from '../shared/shared.variables';

@Injectable()
export class OrientadorDadosService {

  constructor(private _http: HttpClient, public appService: AppService) { }

  retornaQtdProjeto(_funcional : any) {
    let options = {
        headers: new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded'
        })
    }
    return this._http.get(`${baseUrl}Professor/QtdProjetos?numeroFuncional=${_funcional}`, options);
  }

  retornar(_funcional: string): any {
    let options = {
      headers: new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded'
      })
  }
  return this._http.get(`${baseUrl}Professor/GetProfessorByNumeroFuncional?numeroFuncional=${_funcional}`, options);
  }

  alterar(_form: any): any {
    let options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
  }
  return this._http.post(`${baseUrl}Professor/AtualizarDados`,_form,options)
  .pipe(map(data => data));
}
  
  SalvarGrupo(_form: any): any {
    let options = {
      headers: new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded'
      })
    }
  return this._http.post(`${baseUrl}Professor/AtualizarDados`,_form, options)
  .pipe(map(data => data));
  }

  listarGrupo(_funcional: string): any {
    let options = {
      headers: new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded'
      })
    }
    return this._http.get(`${baseUrl}GrupoPesquisa/Listar`, options);
  }

  retornarGrupo(_funcional: string): any {
    let options = {
      headers: new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded'
      })
    }
    return this._http.get(`${baseUrl}GrupoPesquisa/Retornar`, options);
  }

  areaAtuacaoOrientador(_funcional: string, idGrupo: number): any {
    let options = {
      headers: new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded'
      })
    }
    return this._http.get(`${baseUrl}AreaAtuacao/Orientador?numeroFuncional=${_funcional}&idGrupo=${idGrupo}`, options);
  }

  postAreaAtuacaoOrientador(obj): any {
    let options = {
      headers: new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded'
      })
    }

    

    return this._http.post(`${baseUrl}AreaAtuacao/InsereOrientador`, obj, options);
  }

}