import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrientadorDadosComponent } from './orientador-dados.component';

describe('OrientadorDadosComponent', () => {
  let component: OrientadorDadosComponent;
  let fixture: ComponentFixture<OrientadorDadosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrientadorDadosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrientadorDadosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
