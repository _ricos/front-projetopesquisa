

// Imports of libs
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

// Import of modules, components and services
import { OrientadorDadosComponent } from './orientador-dados.component';
import { RouterModule } from '@angular/router';
import { VisualizacaoProjetoComponent } from './visualizacao-projeto/visualizacao-projeto.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule
  ],
  exports: [ OrientadorDadosComponent, VisualizacaoProjetoComponent ],
  providers: [
  ],
  declarations: [ 
    OrientadorDadosComponent, VisualizacaoProjetoComponent
  ]
})
export class OrientadorDadosModule { }
