// Import of libs
import { Component, OnInit } from '@angular/core';

import { AppService } from '../../app.service';
import { VisualizacaoProjetoService } from './visualizacao-projeto.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-visualizacao-projeto',
  templateUrl: './visualizacao-projeto.component.html',
  styleUrls: ['./visualizacao-projeto.component.scss']
})
export class VisualizacaoProjetoComponent implements OnInit {
  qtdProjetos: number;
  idProjeto: number;

  constructor(
    public _appService: AppService,
    public _visualizacaoProjetoService: VisualizacaoProjetoService,
    public route: ActivatedRoute
  ) {
    this.route.params.subscribe((params: any) => {
      this.idProjeto = params['id'];
    });
  }

  ngOnInit() {

    
    this._visualizacaoProjetoService.retornaQtdProjeto(232).subscribe((data : any) => {
      this.qtdProjetos = data;
    }, (error: any) => {
      console.log(error);
    })
  }
}
