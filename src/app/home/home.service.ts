import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppService } from '../app.service';
import { baseUrl } from '../shared/shared.variables';

@Injectable()
export class HomeService {

  constructor(private _http: HttpClient, public appService: AppService) { }

  retornaQtdProjeto(_funcional : any) {
    let options = {
        headers: new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded'
        })
    }
    return this._http.get(`${baseUrl}Professor/QtdProjetos?numeroFuncional=${_funcional}`, options);
  }

  QtdProjetosPareceristaIterno(_funcional : any) {
    let options = {
        headers: new HttpHeaders({
            'Content-Type': 'application/x-www-form-urlencoded'
        })
    }
    return this._http.get(`${baseUrl}ProjetoPesquisa/QtdProjetosPareceristaIterno?numeroFuncional=${_funcional}`, options);
  }

  
}