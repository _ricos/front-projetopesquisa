// Import of libs
import { Component, OnInit } from '@angular/core';

import { AppService } from '../app.service';
import { HomeService } from './home.service';
import { OrientadorDadosService } from '../orientador-dados/orientador-dados.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  qtdProjetos : number;
  qtdProjetosParecerista: number;
  orientador : any;
  pareceristaInterno: boolean;
  numeroFuncional: any = this._appService.numeroFuncional;
  gridActive: boolean = true;
  logado: any;
  _serviceSubscription: any;

  constructor(public _appService: AppService, public _homeService: HomeService, public _orientadorService: OrientadorDadosService,private _router: Router) {
    this._serviceSubscription = this._appService.notifyParent.subscribe({
      next: (event: any) => {
          this.numeroFuncional = event;
          this.inicializar();
      }
  })
   }

  ngOnInit() {
    this.inicializar();
    // this._appService.showProgressBar();
  }
  
  RetornaqtdProjetos(){
    this._homeService.retornaQtdProjeto(this.numeroFuncional).subscribe((data : any) => {      
      this.qtdProjetos = data;
      // this._appService.hideProgressBar();
    }, (error: any) => {
      // this._appService.hideProgressBar();
      console.log(error);
    })
  }

    QtdProjetosPareceristaIterno(){
      this._homeService.QtdProjetosPareceristaIterno(this.numeroFuncional).subscribe((data : any) => {      
        this.qtdProjetosParecerista = data;
        this._appService.hideProgressBar();
      }, (error: any) => {
        this._appService.hideProgressBar();
        console.log(error);
      })
  }

  inicializar(){
    this._orientadorService.retornar(this.numeroFuncional).subscribe((data : any) => {
      
      this.orientador = data;

      // Verifica se o orientador é parecerecista
      this.pareceristaInterno = this.orientador.PareceristaInterno;

      this.RetornaqtdProjetos();
      this.QtdProjetosPareceristaIterno();
      
    }, (error: any) => {
      this._appService.hideProgressBar();
      localStorage.removeItem('token');
      localStorage.removeItem('numeroFuncional');
      this._appService.isLogged = false;
      this._router.navigate(["/access-denied"]);
      console.log(error);
    })
  }
}
