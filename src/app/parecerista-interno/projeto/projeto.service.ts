import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppService } from '../../app.service';
import { baseUrl } from '../../shared/shared.variables';

@Injectable()
export class ProjetoService {

  constructor(
    private _http: HttpClient,
    public appService: AppService
  ) { }

  retornaOrientador(_numeroFuncional) {
    return this._http.get(`${baseUrl}Professor/GetProfessorByNumeroFuncional?numeroFuncional=${_numeroFuncional}`, this.appService.headerOptions);
  }

  getProjetoPesquisa(protocolo: any) {
    return this._http.get(`${baseUrl}ProjetoPesquisa/Retornar?protocolo=${protocolo}`, this.appService.headerOptions);
  }

  retornaHistoricoEscolar(ra: any) {
    return this._http.get(`${baseUrl}ProjetoPesquisa/Historico?ra=${ra}`, this.appService.headerOptions);
  }

  retornaParecerOrientador(protocolo: any) {
    return this._http.get(`${baseUrl}ParecerOrientadorAluno/Retornar?protocolo=${protocolo}`, this.appService.headerOptions);
  }

  retornaParecerParecerista(protocolo: any) {
    return this._http.get(`${baseUrl}ProjetoPesquisa/RespostaPareceristaInterno?protocolo=${protocolo}`, this.appService.headerOptions);
  }

  listarPerguntaResposta() {
    return this._http.get(`${baseUrl}Ranking/ListarPerguntaResposta`, this.appService.headerOptions);
  }

  insereQuestoesParecerista(obj: any) {

    let result: boolean;

    $.ajax({
      type: "POST",
      data: obj,
      url: `${baseUrl}ProjetoPesquisa/PareceristaAvaliacao`,
      success: (msg) => {
        result = true;
      },
      error: (error) => { 
        result = false;
      }
    });

    return result;
  }
}