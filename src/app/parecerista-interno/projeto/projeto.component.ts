// Import of libs
import { Component, OnInit } from '@angular/core';

import { AppService } from '../../app.service';
import { ProjetoService } from './projeto.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { baseUrl } from '../../shared/shared.variables';
import { JsonPipe } from '@angular/common';
import { PareceristaInternoService } from '../parecerista-interno.service';

@Component({
  selector: 'app-projeto',
  templateUrl: './projeto.component.html',
  styleUrls: ['./projeto.component.scss']
})
export class ProjetoComponent implements OnInit {
  baseUrl: string = baseUrl;

  protocolo: number;
  projeto: any;
  aluno: any;
  orientador: any;
  parecerOrientador: any;
  pareceristaInterno: any;
  parecerParecerista: any;
  lstQuestionario: any;

  projetoCarregado: boolean;
  bloqueioSetorPesquisa: boolean = true;
  projetoValidadoOrientador: boolean;
  pareceristaInternoAssociado: boolean; //Verificar se o parecerista interno já validou o projeto para travar.
  historicoCarregado: boolean;
  bloqueioHistorico: boolean = false;
  parecerConcluido: boolean;


  constructor(
    public _appService: AppService,
    public _pareceristaInternoService: PareceristaInternoService,
    public _projetoService: ProjetoService,
    public route: ActivatedRoute,
  
  ) {
    this.route.params.subscribe((params: any) => {
      this.protocolo = params['id'];
    });
  }

  ngOnInit() {
    this.retornaProjetoPesquisa();
    this.retornaParecerOrientador();
  }

  retornaProjetoPesquisa() {
    this._projetoService.getProjetoPesquisa(this.protocolo)
      .subscribe((data: any) => {
        this.projeto = data;
        this.projetoCarregado = true;
        this.pareceristaInternoAssociado = data.PareceristaInterno != ''
        this.retornaParecerParecerista();
        this.retornaHistorico();
      }, (error: any) => {
        this._appService.showToastrError("Não foi possível realizar a busca do projeto.");
        return;
      });
  }

  retornaParecerOrientador() {
    this._projetoService.retornaParecerOrientador(this.protocolo)
      .subscribe((data: any) => {
        this.parecerOrientador = data.Parecer;
        this.projetoValidadoOrientador = data.Validado;
      }, (error: any) => {
        this._appService.showToastrError("Não foi possível retornar o parecer do orientador.");
        return;
      });
  }

  retornaParecerParecerista() {
    this._projetoService.retornaParecerParecerista(this.protocolo)
      .subscribe((data: any) => {
        this.parecerParecerista = data;
        this.parecerConcluido = data.length > 0

        if (!this.parecerConcluido)
          this.listarPerguntaResposta();

      }, (error: any) => {
        this._appService.showToastrError("Não foi possível retornar o parecer do parecerista interno.");
        return;
      });
  }

  retornaHistorico() {
    this._projetoService.retornaHistoricoEscolar(this.projeto.Ra)
      .subscribe((data: any) => {
        this.aluno = data;
        this.historicoCarregado = true;
        this.bloqueioHistorico = false;
      }, (error: any) => {
        this.historicoCarregado = true;
        this.bloqueioHistorico = true;
        this._appService.showToastrError("Não foi possível localizar seu histórico escolar.");
        return;
      });
  }

  listarPerguntaResposta() {
    this._projetoService.listarPerguntaResposta()
      .subscribe((data: any) => {
        this.lstQuestionario = data;
      }, (error: any) => {
        this._appService.showToastrError("Não foi possível localizar o questionário do parecerista interno.");
        return;
      });
  }

  enviarResposta() {

    let error = this.validaForm();

    if(error){
      this._appService.showToastrError("Todos os campos devem ser preenchidos.");
      return;
    }

    let Questoes = [];
    $('#form-cadastro input[type="radio"]:checked').each((index, obj) => {

      let id = $(obj).attr('id');
      let val = $(obj).val().toString().split(',');

      let _perguntaId = val[0];
      let _respostaId = val[1];

      Questoes.push({
        PerguntaId: _perguntaId,
        RespostaId: _respostaId
      });
    });

    let body = {
      NumeroFuncional: localStorage.getItem("numeroFuncional"),
      Protocolo: this.protocolo,
      Questoes: Questoes
    }

    let result: boolean =  this._projetoService.insereQuestoesParecerista(body);

  
    this._appService.showToastrSuccess("Formulário enviado com sucesso!");
    this.retornaProjetoPesquisa();

  }

  validaForm() {
    let error: boolean;

    $('#form-cadastro input[type="radio"]').each((index, obj) => {
      let nameObj = $(obj).attr('name');
      if (!$("input[name='" + nameObj + "']:checked").val()) {
        error = true;
        return ;
      }
    });

    return error;
  }
}
