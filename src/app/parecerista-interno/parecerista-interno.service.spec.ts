import { TestBed, inject } from '@angular/core/testing';
import { PareceristaInternoService } from './parecerista-interno.service';


describe('OrientadorDadosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PareceristaInternoService]
    });
  });

  it('should be created', inject([PareceristaInternoService], (service: PareceristaInternoService) => {
    expect(service).toBeTruthy();
  }));
});
