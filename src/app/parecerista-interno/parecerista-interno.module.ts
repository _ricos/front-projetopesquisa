

// Imports of libs
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';

// Import of modules, components and services
import { PareceristaInternoComponent } from './parecerista-interno.component';
import { RouterModule } from '@angular/router';
import { ProjetoComponent } from './projeto/projeto.component';
import { ProjetoService } from './projeto/projeto.service';
import { SharedModule } from '../shared/shared.module';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    NgxPaginationModule,
  ],
  exports: [ PareceristaInternoComponent ],
  providers: [
    ProjetoService
  ],
  declarations: [ 
    PareceristaInternoComponent,ProjetoComponent
  ]
})
export class PareceristaInternoModule { }
