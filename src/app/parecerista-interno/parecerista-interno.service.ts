import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppService } from '../app.service';
import { FormGroup } from '@angular/forms';
import { map } from "rxjs/operators";
import { baseUrl } from '../shared/shared.variables';

@Injectable()
export class PareceristaInternoService {

  constructor(private _http: HttpClient, public appService: AppService) { }
  
 listaProjetosPareceristaInterno(_funcional : any){
    let options = {
      headers: new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded'
      })
  }
  return this._http.get(`${baseUrl}ProjetoPesquisa/ProjetosPareceristaInterno?numeroFuncional=${_funcional}`, options);
  }

}