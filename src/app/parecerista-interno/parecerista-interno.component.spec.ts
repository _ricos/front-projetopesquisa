import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PareceristaInternoComponent } from './parecerista-interno.component';

describe('PareceristaInternoComponent', () => {
  let component: PareceristaInternoComponent;
  let fixture: ComponentFixture<PareceristaInternoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PareceristaInternoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PareceristaInternoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
