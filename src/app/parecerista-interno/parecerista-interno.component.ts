// Import of libs
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { AppService } from '../app.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ConnectionBackend } from '@angular/http';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import * as $ from 'jquery';
import { identifierModuleUrl } from '@angular/compiler';
import { baseUrl } from '../shared/shared.variables';
import { PareceristaInternoService } from './parecerista-interno.service';


@Component({
  selector: 'app-parecerista-interno',
  templateUrl: './parecerista-interno.component.html',
  styleUrls: ['./parecerista-interno.component.scss']
})
export class PareceristaInternoComponent implements OnInit {

  p: number = 1;
  listaProjeto: any = [];
  filtrado: any;

  constructor(
    public _appService: AppService,
    public _pareceristaInternoService: PareceristaInternoService,
    public route: ActivatedRoute,
    private _location: Location,
    public router: Router
  ) {
   
  }

  ngOnInit() {
    this._pareceristaInternoService.listaProjetosPareceristaInterno(localStorage.getItem("numeroFuncional")).subscribe(
      (data : any) => {
        this.listaProjeto = data;
        this.filtrado = data;
    }, (error: any) => {
    })
  }

  filtrarProjetos(event: any){
    let srtFiltro = this._appService.normalizarString(event.target.value.toLowerCase());

    this.filtrado = this.listaProjeto.filter((el) => {
      return this._appService.normalizarString(el.Ra.toLowerCase()).indexOf(srtFiltro) > -1||
             this._appService.normalizarString(el.Protocolo.toLowerCase()).indexOf(srtFiltro) > -1||
             this._appService.normalizarString(el.Aluno.toLowerCase()).indexOf(srtFiltro) > -1||
             this._appService.normalizarString(el.Email.toLowerCase()).indexOf(srtFiltro) > -1||
             this._appService.normalizarString(el.EmailOrientador.toLowerCase()).indexOf(srtFiltro) > -1||
             this._appService.normalizarString(el.Orientador.toLowerCase()).indexOf(srtFiltro) > -1||
             this._appService.normalizarString(el.Situacao.toLowerCase()).indexOf(srtFiltro) > -1||
             this._appService.normalizarString(el.Turma.toLowerCase()).indexOf(srtFiltro) > -1||
             this._appService.normalizarString(el.Campus.toLowerCase()).indexOf(srtFiltro) > -1||
             this._appService.normalizarString(el.TipoAluno.toLowerCase()).indexOf(srtFiltro) > -1;
     });
  }
}
