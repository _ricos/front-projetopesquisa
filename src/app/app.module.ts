// Import of libs
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import * as $ from 'jquery';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpClientModule } from '@ngx-progressbar/http-client';

// Import of modules, components and services
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { SharedModule } from './shared/shared.module';
import { HomeModule } from './home/home.module';
import { AppService } from './app.service';
import { HttpClientModule } from '@angular/common/http';
import { HomeService } from './home/home.service';
import { ListaProjetosModule } from './lista-projetos/lista-projetos.module';
import { ListaProjetosService } from './lista-projetos/lista-projetos.service';
import { NotFoundComponent } from './not-found/not-found.component';
import { OrientadorDadosModule } from './orientador-dados/orientador-dados.module';
import { OrientadorDadosService } from './orientador-dados/orientador-dados.service';
import { PareceristaInternoService } from './parecerista-interno/parecerista-interno.service';
import { PareceristaInternoModule } from './parecerista-interno/parecerista-interno.module';
import { PareceristaProjetosModule } from './parecerista-projetos/parecerista-projetos.module';
import { AccessNotAllowedComponent } from './access-not-allowed/access-not-allowed.component';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    AccessNotAllowedComponent,
  ],
  imports: [
    BrowserModule,
    CommonModule,
    HttpClientModule,
    // Import modules of application
    PareceristaProjetosModule,
    SharedModule,
    HomeModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    ListaProjetosModule,
    OrientadorDadosModule,
    PareceristaInternoModule,
    PareceristaProjetosModule,
    PareceristaInternoModule,
    AppRoutingModule,
    NgProgressModule.forRoot(),
    NgProgressHttpClientModule
  ],
  exports: [ ],
  providers: [ AppService, HomeService,ListaProjetosService, OrientadorDadosService,PareceristaInternoService ],
  bootstrap: [ AppComponent ]

})
export class AppModule { }
