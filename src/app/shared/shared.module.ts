// Imports of libs
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Import of modules, components and services
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SharedComponent } from './shared.component';
import { RouterModule } from '@angular/router';
import { MyDatePipe } from './pipes/my-date-pipe';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [ HeaderComponent, FooterComponent, SharedComponent, MyDatePipe ], 
  declarations: [ HeaderComponent, FooterComponent, SharedComponent,MyDatePipe ]
})
export class SharedModule { }
