// Imports of libs
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Imports of components, modules and service
import { HomeComponent } from './home/home.component';
import { ListaProjetosComponent } from './lista-projetos/lista-projetos.component';
import { OrientadorDadosComponent } from './orientador-dados/orientador-dados.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { VisualizacaoProjetoComponent } from './lista-projetos/visualizacao-projeto/visualizacao-projeto.component';
import { PareceristaInternoComponent } from './parecerista-interno/parecerista-interno.component';
import { ProjetoComponent } from './parecerista-interno/projeto/projeto.component';
import { AccessNotAllowedComponent } from './access-not-allowed/access-not-allowed.component';

const appRoutes : Routes = [
  { path: 'lista-projetos/:id', component: VisualizacaoProjetoComponent }, 
  { path: 'lista-projetos', component: ListaProjetosComponent},
  { path: 'home', component: HomeComponent },
  { path: 'orientador-dados', component: OrientadorDadosComponent },  
  { path: 'parecerista-interno/:id', component: ProjetoComponent},
  { path: 'parecerista-interno', component: PareceristaInternoComponent},
  { path: '', redirectTo: "home", pathMatch: "full" },
  { path: 'access-denied', component: AccessNotAllowedComponent },
]

@NgModule({
  imports: [ RouterModule.forRoot(appRoutes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule { }