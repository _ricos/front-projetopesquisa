import { Injectable, Output, EventEmitter } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { FormGroup } from '@angular/forms';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { NgProgress } from '@ngx-progressbar/core';
import { baseUrl } from './shared/shared.variables';

@Injectable()
export class AppService {
  @Output() notifyParent: EventEmitter<any> = new EventEmitter();

  componentRef: any; // Reference to destroy element
  elementRef: any; // receive the element
  isLogged: boolean = false;
  token: any = this.getUrlParameter('token', location.href) || localStorage.getItem('token');
  numeroFuncional = localStorage.getItem("numeroFuncional");
  private windowWidth;
  public loaderIsActive: boolean = false;
  headerOptions;
  headerOptionsJson;

  constructor(
    public toastr: ToastrService,
    public ngProgress: NgProgress,
    private _http: HttpClient,
  ) {
    this.headerOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    }

    this.headerOptionsJson = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    }

    if (this.token != null) {
      localStorage.setItem('token', this.token);
    }
  }

  ngOnInit() { }

  tratarObjeto(obj) {
    var newObj = '';
    let array = Object.getOwnPropertyNames(obj);
    $.each(array, (index, value) => { newObj += value + '=' + obj[value] + "&" })
    return newObj;
  }

  showToastrSuccess(msg: string) {
    this.toastr.success(msg);
  }

  showToastrWarning(msg: string) {
    this.toastr.warning(msg);
  }

  showToastrError(msg: string) {
    this.toastr.error(msg);
  }

  normalizarString(a: string) {
    return a = a = a.replace(/[EÉÈÊË]/gi, "E"),
      a = a.replace(/[eéèêë]/gi, "e"),
      a = a.replace(/[AÀÁÂÃÄÅÆ]/gi, "A"),
      a = a.replace(/[aàáãâä]/gi, "a"),
      a = a.replace(/[cç]/gi, "c"),
      a = a.replace(/[IÌÍÎÏ]/gi, "I"),
      a = a.replace(/[iíìïî]/gi, "i"),
      a = a.replace(/[ÒÓÔÕÖ]/gi, "O"),
      a = a.replace(/[oóòôö]/gi, "o"),
      a = a.replace(/[UÜÛÙÚ]/gi, "U"),
      a = a.replace(/[uúùüû]/gi, "u")
  }

  verificaValidacoesForm(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach((campo) => {
      var controle = formGroup.get(campo);
      controle.markAsTouched();
      if (controle instanceof FormGroup) {
        this.verificaValidacoesForm(controle);
      }
    });
  }

  getRandomNumber(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  isDesktop() {
    this.windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    return this.windowWidth >= 768 ? true : false;
  }

  isMobile() {
    this.windowWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    return this.windowWidth < 768 ? true : false;
  }

  getUrlParameter(name, url) {
    if (!url) url = location.href;
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);
    return results == null ? null : results[1];
  }

  showProgressBar() {
    this.ngProgress.start();
    this.loaderIsActive = true;
  }

  hideProgressBar() {
    if (this.loaderIsActive)
      this.ngProgress.done();
    this.loaderIsActive = false;
  }

  autenticaOrientador(obj: any) {
    let options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    }

    return this._http.post(`${baseUrl}Professor/Autenticar`, this.tratarObjeto(obj), options);
  }

  initTooltip() {
    setTimeout(() => {
      // Funcao para criar tooltips de informacao
    $(function () {
      (<any>$('[data-toggle="tooltip"]')).tooltip()
    })
    // Funcao para chamar modal de historico escolar
    $('#modalHistorico').on('shown.bs.modal', function () {
      $('#historico-escolar-modal').trigger('focus')
    })
    // Funcao para chamar modal de resumo projeto
    $('#modalResumo').on('shown.bs.modal', function () {
      $('#resumo-projeto-modal').trigger('focus')
    })
    }, 100);
  }
}