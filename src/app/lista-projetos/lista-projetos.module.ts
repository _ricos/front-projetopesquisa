

// Imports of libs
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Import of modules, components and services
import { ListaProjetosComponent } from './lista-projetos.component';
import { RouterModule } from '@angular/router';
import { VisualizacaoProjetoComponent } from './visualizacao-projeto/visualizacao-projeto.component';
import { VisualizacaoProjetoService } from './visualizacao-projeto/visualizacao-projeto.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { MyDatePipe } from '../shared/pipes/my-date-pipe';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    NgxPaginationModule,
    SharedModule,
  ],
  exports: [ ListaProjetosComponent ],
  providers: [
    VisualizacaoProjetoService,
    MyDatePipe
  ],
  declarations: [
    ListaProjetosComponent,
    VisualizacaoProjetoComponent
  ]
})
export class ListaProjetosModule { }
