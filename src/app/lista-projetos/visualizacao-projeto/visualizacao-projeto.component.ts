// Import of libs
import { Component, OnInit } from '@angular/core';

import { AppService } from '../../app.service';
import { VisualizacaoProjetoService } from './visualizacao-projeto.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { baseUrl } from '../../shared/shared.variables';

@Component({
  selector: 'app-visualizacao-projeto',
  templateUrl: './visualizacao-projeto.component.html',
  styleUrls: ['./visualizacao-projeto.component.scss']
})
export class VisualizacaoProjetoComponent implements OnInit {
  qtdProjetos: number;
  currentProtocolo: number;
  formProject: FormGroup;
  aluno: any;
  parecer: string = '';
  projetoValidado: boolean = false;
  projectAlreadyValidated: boolean = true;
  projeto: any;
  baseUrl: string = baseUrl;
  projectLoaded: boolean = false;
  historicoCarregado: boolean;
  bloqueioHistorico: boolean = false;

  constructor(
    public _appService: AppService,
    public _visualizacaoProjetoService: VisualizacaoProjetoService,
    public route: ActivatedRoute,
    private formBuilder: FormBuilder
  ) {
    this.route.params.subscribe((params: any) => {
      this.currentProtocolo = params['id'];
    });
  }

  ngOnInit() {

    console.log(this.currentProtocolo);



    this.formProject = this.formBuilder.group({
      Parecer: [this.parecer, [Validators.required, Validators.minLength(5)]],
      Validado: [this.projetoValidado],
      NumeroFuncional : [this._appService.numeroFuncional],
      Protocolo : [this.currentProtocolo]
    });

    console.log(this.currentProtocolo);

    console.log("chegou");
    this._visualizacaoProjetoService.getProjetoPesquisa(this.currentProtocolo)
      .subscribe((data : any) => {
        this.projeto = data;   
        this.retornaHistorico();
        this.projectLoaded = true;
        
      }, (error: any) => {
        console.log(error);
      })

    

    this._visualizacaoProjetoService.getProjetoParecer(this.currentProtocolo)
      .subscribe((data : any) => {
        this.parecer = data.Parecer;
        this.projetoValidado = data.Validado;
        
        if(data.NumeroFuncional != null) {
          this.projectAlreadyValidated = true;
        } else {
          this.projectAlreadyValidated = false;
        }
          
      }, (error: any) => {
        console.log(error);
      })
  }

  onSubmit() {

    if(this.formProject.valid) {
      this._visualizacaoProjetoService.postParecer(this.formProject.value)
        .subscribe((data : any) => {
          this.projectAlreadyValidated = true;
          this._appService.showToastrSuccess(`Formulário enviado com sucesso`);          
        }, (res: any) => {       
          this._appService.showToastrError(`Ocorreu algum erro inesperado`);
        });
        
    } else { // formulário inválido
      $('input.ng-invalid').eq(0).focus();
      this._appService.showToastrWarning(`Para prosseguir, é necessário preencher todos os campos`);
      this._appService.verificaValidacoesForm(this.formProject);
    }
  }

  
  retornaHistorico() {
    this._visualizacaoProjetoService.retornaHistoricoEscolar(this.projeto.Ra)
      .subscribe((data: any) => {
        this.aluno = data;
        this.historicoCarregado = true;
        this.bloqueioHistorico = false;
      }, (error: any) => {
        this.historicoCarregado = true;
        this.bloqueioHistorico = true;
        this._appService.showToastrError("Não foi possível localizar seu histórico escolar.");
        return;
      });
  }
}