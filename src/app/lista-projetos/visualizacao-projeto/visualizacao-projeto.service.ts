import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppService } from '../../app.service';
import { baseUrl } from '../../shared/shared.variables';

@Injectable()
export class VisualizacaoProjetoService {

  constructor(
    private _http: HttpClient, 
    public appService: AppService
  ) { }

  retornaQtdProjeto(_projetoId : any) {
    return this._http.get(`${baseUrl}Professor/?projetoId=${_projetoId}`, this.appService.headerOptions);
  }

  getProjetoPesquisa(protocolo : any) {
    return this._http.get(`${baseUrl}ProjetoPesquisa/Retornar?protocolo=${protocolo}`, this.appService.headerOptions);
  }

  getProjetoParecer(protocolo : any) {
    return this._http.get(`${baseUrl}ParecerOrientadorAluno/Retornar?protocolo=${protocolo}`, this.appService.headerOptions);
  }

  postParecer(obj): any {
    return this._http.post(`${baseUrl}ParecerOrientadorAluno/Inserir`, this.appService.tratarObjeto(obj), this.appService.headerOptions);
  }

  retornaHistoricoEscolar(ra: any) {
    return this._http.get(`${baseUrl}ProjetoPesquisa/Historico?ra=${ra}`, this.appService.headerOptions);
  }
}