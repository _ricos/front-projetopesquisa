import { TestBed, inject } from '@angular/core/testing';

import { ListaProjetosService } from './lista-projetos.service';

describe('ListaProjetosService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListaProjetosService]
    });
  });

  it('should be created', inject([ListaProjetosService], (service: ListaProjetosService) => {
    expect(service).toBeTruthy();
  }));
});
