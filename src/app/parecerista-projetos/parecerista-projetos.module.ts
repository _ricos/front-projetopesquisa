import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PareceristaVisualizaProjetosComponent } from './parecerista-visualiza-projetos/parecerista-visualiza-projetos.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RouterModule } from '@angular/router';
import { PareceristaProjetosService } from './parecerista-projetos.service';
import { PareceristaProjetosComponent } from './parecerista-projetos.component';
import { PipeDater } from './daterPipe';

@NgModule({
  declarations: [ PipeDater, PareceristaVisualizaProjetosComponent, PareceristaProjetosComponent],
  imports: [
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [PareceristaProjetosService],
  exports: [PareceristaVisualizaProjetosComponent]

})
export class PareceristaProjetosModule { }
