import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pipeDater'
})

export class PipeDater implements PipeTransform {
  transform(value: string): any {
    return new Date(parseInt(value.substr(6)));
  }
}