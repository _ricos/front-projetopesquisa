import { TestBed } from '@angular/core/testing';

import { PareceristaProjetosService } from './parecerista-projetos.service';

describe('PareceristaProjetosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PareceristaProjetosService = TestBed.get(PareceristaProjetosService);
    expect(service).toBeTruthy();
  });
});
