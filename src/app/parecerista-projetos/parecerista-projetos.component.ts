
import { Component, OnInit, Pipe, PipeTransform  } from '@angular/core';

import { AppService } from '../app.service';
import { PareceristaProjetosService } from './parecerista-projetos.service';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-parecerista-projetos',
  templateUrl: './parecerista-projetos.component.html',
  styleUrls: ['./parecerista-projetos.component.scss']
})

export class PareceristaProjetosComponent implements OnInit {
  qtdProjetos: number;
  listaProjetos: any;
  constructor(
    public _appService: AppService,
    public _PareceristaProjetosService: PareceristaProjetosService,
    public route: ActivatedRoute,
    public router: Router
  ) { }

  ngOnInit() {
    this.requestProjects();

    this.router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        if (this.route.children.length === 0) {
        }
      }
    });
  }

  requestProjects() {
    this._PareceristaProjetosService.listaProjeto = [];
    this._PareceristaProjetosService.listaProjetos(localStorage.getItem("numeroFuncional")).subscribe(
      (data: any) => {
        this._PareceristaProjetosService.listaProjeto = data;
        console.log(data);
      },
      (error: any) => {
        console.log(error);
      });
  }
}
