import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PareceristaVisualizaProjetosComponent } from './parecerista-visualiza-projetos.component';

describe('PareceristaVisualizaProjetosComponent', () => {
  let component: PareceristaVisualizaProjetosComponent;
  let fixture: ComponentFixture<PareceristaVisualizaProjetosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PareceristaVisualizaProjetosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PareceristaVisualizaProjetosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
