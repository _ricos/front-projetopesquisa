import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PareceristaProjetosComponent } from './parecerista-projetos.component';

describe('PareceristaProjetosComponent', () => {
  let component: PareceristaProjetosComponent;
  let fixture: ComponentFixture<PareceristaProjetosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PareceristaProjetosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PareceristaProjetosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
