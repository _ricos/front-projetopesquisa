import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppService } from '../app.service';
import { baseUrl } from '../shared/shared.variables';

@Injectable({
  providedIn: 'root'
})
export class PareceristaProjetosService {
  listaProjeto: any = [];
  constructor(private _http: HttpClient, public appService: AppService) { }

  listaProjetos(_funcional: any) {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
      })
    };
    return this._http.get(`${baseUrl}ProjetoPesquisa/ProjetosOrientador?numeroFuncional=544680`, options);
  }
}
