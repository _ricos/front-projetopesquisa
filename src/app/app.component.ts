// Import libs
import { Component, Output, EventEmitter } from '@angular/core';

// Import components, modules and service
import { AppService } from './app.service';
import { Router, NavigationEnd } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  componentRef: any;
  private _serviceSubscription;

  constructor(
    public appService: AppService,
    private _router: Router
  ) {
  }

  ngOnInit() {
    this._router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        window.scrollTo(0, 0);

        let token = localStorage.getItem("token");
        let funcional = localStorage.getItem("numeroFuncional");

        this.appService.initTooltip();

        if (val.url.split('?')[0] == '/home' && val.url.indexOf("token") != -1) {
          if (token != undefined) {
            let body = {
              token: token
            }

            this.appService.autenticaOrientador(body).subscribe((data: any) => {
              this.appService.notifyParent.emit(data.NumeroFuncional);

              let orientador = data;

              if (orientador.NumeroFuncional == undefined) {
                this._router.navigate(["/access-denied"]);
                return;
              }

              localStorage.setItem('numeroFuncional', orientador.NumeroFuncional);
              this.appService.numeroFuncional = orientador.NumeroFuncional;
              this.appService.isLogged = true;

            }, (error: any) => {
              this._router.navigate(["/access-denied"]);
              return;
            });
          } else {
            this._router.navigate(["/access-denied"]);
          }
        } else {

          if ((token == undefined || token == null) || (funcional == undefined || funcional == null)) {
            localStorage.removeItem('token');
            localStorage.removeItem('numeroFuncional');
            this.appService.isLogged = false;
            this._router.navigate(["/access-denied"]);
            return;
          }
        }
      }
    });
  }
}